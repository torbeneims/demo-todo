---
type: slide
slideOptions:
transition: slide
theme: uncover
class: invert


---
JavaScript Basics
===
### and their application in the development and deployment of a simple todo app using Vue and Firebase Hosting

---

## Webstorm
![width:100px](https://resources.jetbrains.com/storage/products/company/brand/logos/WebStorm_icon.svg?_ga=2.172701665.37992340.1647953320-1951118302.1628703379&_gl=1*nljbey*_ga*MTk1MTExODMwMi4xNjI4NzAzMzc5*_ga_V0XZL7QHEB*MTY0Nzk1MzMxOS4yOS4xLjE2NDc5NTM1MTEuMA..)

## NodeJS
![width:200px](https://upload.wikimedia.org/wikipedia/commons/d/d9/Node.js_logo.svg)

---

## Basics
```js
// No main class

let myVar = 5;             // camelCase
const myConstant = 1       // <- semicolons are optional but encouraged
let arr = [1, 2, 3, ];     // arrays and objects may have trailing commas
arr[1] // 2
let obj = {prop: 'hello'}; // single quotes, " is possible but discouraged
obj.prop // 'hello'

console.log('Hello World!');

class myClass {}
```

---

## Control structure
```js
if (cond) {
    myFunc();
} else
    x = 3; // discouraged, use braces

for (let i = 0; i < x; i++) {
    continue;
}
for (i of [1, 2, 3]) {}       // 1, 2, 3
for (key in {a: 1, b: 2}) {} // 'a', 'b'

while (cond) {}
do {} while (cond);
```


---

## Functions and Formatting
```js
function myFunc(param) {}
const obj = {
    myFunc1() {},
    myFunc2: function() {},
    myFunc3: () => {} // Careful with the this-context inside arrow-functions
};

let formattedString = `Iteration: ${i}`
```

---

## Computed Properties and Accessors
```js
let propName = 'myProp';
let data ={
    'a': 'b',                    // JS allows quoted property names
    foo: 'bar',
    [propName]: 'computedProp'
};
data.a // 'b'
data['foo'] // 'bar'
data.myProp // 'computedProp'
```

---

## Destructuring
```js
const arr = ['🥓', '🍕', undefined, '🍔', '🥩']
const [bacon,, fries = '🥔', ...rest] = arr
bacon // '🥓'
pizza // undefined
fries // '🥔' 
rest // ['🍔', '🥩']

const obj = {banana: '🍌', 'my shroom': '🍄', tomato: '🍅'}
const { 'my shroom': mushroom, banana = 'long yellow thing' } = obj
mushroom // '🍄'

const family = {parent: {child: '👶🏻'}}
const {parent: { child }} = family
child // '👶🏻'
```


---

## Destructuring Use Cases
```js
// swapping
const a = 420, b = 187
[a, b] = [b, a]
a // 187
b // 420

for ({ name, age } of objects) {}
function myFunc({id, username}) {}
myFunc({username: 'joe', id: 3})
const [ foo, bar ] = 'do re mi la'.match(/\w+\s/g)

const obj = { [rando]: 23 }
const { [rando]: myKey } = obj
myKey // 23
```

---

## Basic Array Methods
```js
const arr = ['🐇', '🦘', '🦔', '🐢', '🐒']

arr.length // 5
    
['🐨', ...arr] // ['🐨', '🐇', '🦘', '🦔', '🐢', '🐒']

arr.sort()
arr.sort((a, b) => a - b)

arr.includes('🐨') // false

arr.find((value, index, array) => value === '🐢') // '🐢'
arr.findIndex((value, index, array) => value === '🦔') // 2
arr.indexOf('🦔') // 2
```

---

## Advanced Array Methods
```js
const arr = ['🐇', '🦘', '🦔', '🐢', '🐒']
const obj = {rabbit: '🐇', roo: '🦘', monkey: '🐒'}

arr.filter((value, index, array) => value !== '🦘')

arr.forEach((value, index, array) => {})

arr.map((value, index, array) => 'Mascot: ' + value)
arr.reduce((acc, value, index, array) => acc + value, '') // '🐇🦘🦔🐢🐒'

Object.entries(obj).forEach(([key, value]) => {})
// ['rabbit', '🐇'], ['roo', '🦘'], ['monkey', '🐒']
Object.keys(obj).forEach(key => {}) // '🐇', '🦘', '🐒'
Object.values(obj).forEach(value => {}) // 'rabbit', 'roo', 'monkey'
```

---

## Quirks
```js
// arrays are objects
let arr = [1, 2];
arr[-1] = 'a';
arr // [1, 2, '-1': 'a']

'11' + 1 // '111'
'11' - 1 // 10

('b' + 'a' + +'a' + 'a').toLowerCase() // banana

[] == 0 // true
'0' == 0 // true
[] == '0' // false
```


---

## Vue

![width:100px](https://upload.wikimedia.org/wikipedia/commons/9/95/Vue.js_Logo_2.svg)


```sh
npm install --global @vue/cli
```


```sh
vue create <app>
```

----

![](https://i.imgur.com/U1YjKK6.png)

----

```shell
vue add vuetify
```


![](https://i.imgur.com/Cv0zHiN.png)

---

# First Content

----

```html
<p>
  Hello World
</p>
```

----

```html
<v-container class="mt-10">
  <h1 class="display-2 font-weight-bold mb-8">
    Welcome to your Todo List
  </h1>
  <p>
    Get things done
  </p>
</v-container>
```

---

# Reactivity

----

```js
const todoTemplate = {text: "", done: false}
```

```js
data: () => ({
    newTodo: Object.assign({}, todoTemplate),
    todos: [],
}),
```

----

```html
<v-container class="mt-10">
  <h1 class="display-2 font-weight-bold mb-8">
    Welcome to your Todo List
  </h1>
  <p v-if="todos.length === 0">
    You don't appear to have any tasks
  </p>
  <p v-else-if="todos.find(v => !v.done)">
    You have completed {{ todos.reduce((acc, v) => acc + v.done, 0) }} of 
    {{ todos.length }} {{ todos.length !== 1 ? 'tasks' : 'task' }}.
  </p>
  <p v-else>
    You have completed all of your tasks. Great job
  </p>
</v-container>
```

---


# Adding Items

----

```js
methods: {
    addTodo: function () {
      if (!this.newTodo.text)
        return
    
      this.todos.push(this.newTodo)
      
      this.newTodo = Object.assign({}, todoTemplate)
    },
},
```

```html
<v-text-field
  v-model="newTodo.text"
  append-outer-icon="mdi-plus"
  label="Add new Item"
  @keydown.enter="addTodo()"
  @click:append-outer="addTodo()"/>
```

---

# Displaying all Items


----

```html
<v-container class="mt-10">
  <h2 class="headline font-weight-bold mb-3">
    What's left to do
  </h2>

  <v-card
      v-for="item in todos.filter(item => !item.done)"
      :key="item.name"
      class="my-2"
      @click="item.done =! item.done"
  >
    <v-card-title>
      <h4>{{ item.text }}</h4>
    </v-card-title>
  </v-card>
</v-container>
```

---

# Deleting Items

----

```js
methods: {
    clearTodos() {
      this.todos = []
    },
},
```

```html
<v-btn
  :disabled="!todos.length"
  class="ma-2 mt-10 pa-6 white--text"
  color="red"
  @click="clearTodos"
>
    Delete All
    <v-icon right>mdi-delete</v-icon>
</v-btn>
```

---

# Saving the application state

----

```js
watch: {
    // Save the todos to the browser's storage whenever the data is updated
    todos: {
        handler() {
            this.saveState()
        },
        // Do not just look at changes to the array 
        // (e.g. insertions, deletions), but also the contained 
        // objects (e.g. when an item is marked as done)
        deep: true
    }
},
```

```js
mounted() {
    // Fetch the saved todos when the page loads
    if (localStorage.todos)
      this.todos = JSON.parse(localStorage.todos)
},
```

---

# Dark Mode

----

```js
// Get the media query containing the user's preferred theme
const mq = window.matchMedia('(prefers-color-scheme: dark)')

// Instantiate Vuetify using this theme
const vuetify = new Vuetify({
    theme: { dark: mq.matches }
})

// Update the app theme every time the preferred theme is changed
// (potentially by the user, timed night mode, ...)
mq.addEventListener('change', (e) => {
    vuetify.framework.theme.dark = e.matches
})

export default vuetify
```

---

![width:800px](https://i.imgur.com/qc9pdo3.png)
