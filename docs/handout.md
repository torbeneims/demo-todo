
## Basics
```js
// No main class

let myVar = 5;             // camelCase
const myConstant = 1       // semicolons are optional but encouraged
var anotherVar = 2;        // discouraged
let arr = [1, 2, 3, ];     // arrays and objects may have trailing commas
arr[1] // 2
let obj = {prop: 'hello'}; // single quotes, " is possible but discouraged
obj.prop // 'hello'

console.log('Hello World!');

class myClass {}
```

## Control structure
```js
if (cond) {
    myFunc();
} else
    x = 3; // discouraged, use braces
```

```js
// Basic for loop
for (let i = 0; i < x; i++) {
    continue;
}

// Iterate over an array
for (i of [1, 2, 3]) {}       // 1, 2, 3

// Iterate over the keys of an object
for (key in {a: 1, b: 2}) {} // 'a', 'b'

// Iterate over the keys and values of an array
// see destructuring, advanced array methods
for (const [key, value] of Object.entries(obj)) {}
```
```js
// Basic while loop
while (cond) {}

// do-while
do {} while (cond);
```


## Functions
```js
// Basic function declaration
function myFunc(param) {}

// Multiple ways to declare a function within an object
const obj = {
    myFunc1() {},
    myFunc2: function() {},
    myFunc3: () => {} // Careful with the this-context inside arrow-functions
};

// Declare a function and return an object (often used for Vue data)
const obj = {
    myFunc1() {return {}},
    myFunc2: function() {return {}},
    myFunc3: () => ({}) // Careful with the this-context inside arrow-functions
};
```

## String Formatting
```js
let formattedString = `Iteration: ${i}`;
```

## Computed Properties and Accessors
```js
let propName = 'myProp';
let data = {
    'a': 'b',                    // JS allows quoted property names
    foo: 'bar',
    [propName]: 'computedProp'
};
data.a // 'b'
data['foo'] // 'bar'
data.myProp // 'computedProp'
```

## Destructuring
```js
// Basic array destructuring
const arr = ['🥓', '🍕', '🍟', '🍔', '🥩']
const [bacon, pizza, fries, ...rest] = arr

bacon // '🥓'
pizza // '🍕'
fries // '🥔' 
rest // ['🍔', '🥩']

// Skipping a value
const arr = ['🥓', '🍕', '🍟', '🍔', '🥩']
const [bacon,, fries] = arr

bacon // '🥓'
pizza // undefined
fries // '🍟' 

// Default values
const arr = ['🥓', '🍕', undefined, '🍔', '🥩']
const [bacon = '🐖', pizza, fries = '🥔'] = arr

bacon // '🥓'
pizza // '🍕'
fries // '🥔'
```

```js
// Basic object destructuring
let obj = {banana: '🍌', mushroom: '🍄', tomato: '🍅'}
let {mushroom, banana} = obj
mushroom // '🍄'
banana // '🍌'

// Renaming properties
// (Could also be useful as object keys may be invalid variable names)
let obj = {banana: '🍌', 'my shroom': '🍄', tomato: '🍅'}
let { 'my shroom': mushroom, banana } = obj
mushroom // '🍄'
banana // '🍌'

// Default values
const obj = {banana: undefined, 'my shroom': '🍄', tomato: '🍅'}
let { banana = 'long yellow thing', apple = '🍏', carrot } = obj
banana // 'long yellow thing'
apple // '🍏'
carrot // undefined
```

```js
// Nested destructuring
const family = {
    parent: {
        child: '👶🏻'
    }
}
const { parent: { child } } = family
child // '👶🏻'
```


## Destructuring Use Cases
```js
// Swapping
const a = 420, b = 187
[a, b] = [b, a]
a // 187
b // 420
```

```js
// Destructuring in loop headers
for ({ name, age } of objects) {}

// Destructuring in function headers
function myFunc({id, username}) {}
myFunc({username: 'joe', id: 3})

// Destructuring for regex matches
const [ foo, bar ] = 'do re mi la'.match(/\w+\s/g)
foo // 'do '
bar // 're '

// Destructuring computed properties
const obj = { [rando]: 23 }
const { [rando]: myKey } = obj
myKey // 23
```

## Basic Array Methods
```js
const arr = ['🐇', '🦘', '🦔', '🐢', '🐒']

// Length
arr.length // 5
    
// Spread syntax
['🐨', ...arr] // ['🐨', '🐇', '🦘', '🦔', '🐢', '🐒']
```
```js
// Ascending in-place sorting
[3, 2, 6, 1].sort() // [1, 2, 3, 6]

// Custom in-place sorting
[3, 2, 6, 1].sort((a, b) => b - a) // [6, 3, 2, 1]
```
```js
const arr = ['🐇', '🦘', '🦔', '🐢', '🐒']

// Contains
arr.includes('🐨') // false

// Finding values or indices
arr.indexOf('🦔') // 2
arr.find((value, index, array) => value === '🐢') // '🐢'
arr.findIndex((value, index, array) => value === '🦔') // 2

// Predicate
arr.some((value, index, array) => value === '🦘') // true (=any)
arr.every((value, index, array) => value === '🦘') // false (=all)
```

## Advanced Array Methods
```js
const arr = ['🐇', '🦘', '🦔', '🐢', '🐒']
const obj = {rabbit: '🐇', roo: '🦘', monkey: '🐒'}

// Filtering
arr.filter((value, index, array) => value !== '🦘')
// ['🐇', '🦔', '🐢', '🐒']

// Iterating
arr.forEach((value, index, array) => {})

// Mapping
arr.map((value, index, array) => 'Mascot: ' + value)
// ['Mascot: 🐇', 'Mascot: 🦘', 'Mascot: 🦔', 'Mascot: 🐢', 'Mascot: 🐒']

// Reducing to a single value
// The accumulator is updated to the return value of the reducer
// function in each iteration
// The last is the initial accumulator value, defaults to first entry
arr.reduce((acc, value, index, array) => acc + value, 'Animals: ')
// 'Animals: 🐇🦘🦔🐢🐒'

// Iterating the keys and values of objects
Object.entries(obj).forEach(([key, value]) => {})
// ['rabbit', '🐇'], ['roo', '🦘'], ['monkey', '🐒']
Object.keys(obj).forEach(key => {}) // 'rabbit', 'roo', 'monkey'
Object.values(obj).forEach(value => {}) // '🐇', '🦘', '🐒'
```

---

## Quirks
```js
// arrays are objects
let arr = [1, 2];
arr[-1] = 'a';
arr // [1, 2, '-1': 'a']

// Addition is defined for strings, therefore 1 is parsed to a string
'11' + 1 // '111'
'11' - 1 // 10
// Subtraction is not defined for strings, therefore '11' is parsed to a number
    
// + + 'a' results in a NaN error, which is then string- concatenated to the rest
('b' + 'a' + + 'a' + 'a').toLowerCase() // banana

// [] and '0' are coerced to a number (0), therefore the first two statements are true
// In the third statement, both values are coerced to a boolean, true and false, respectively
[] == 0 // true
'0' == 0 // true
[] == '0' // false
```


# TypeScript
```ts
let x: number = 1

const arr: string[] = ['🐇', '🦘', '🦔', '🐢', '🐒']

let obj: {banana: string, mushroom: string, tomato: string} = {banana: '🍌', mushroom: '🍄', tomato: '🍅'}
obj.pear // error

type fruit_basket = {banana: string, mushroom: string, tomato: string}
let obj2: {banana: string, mushroom: string, tomato: string} = {banana: '🍌', mushroom: '🍄', tomato: '🍅'}
obj2.pear // error

function myFunc(param: string): number {
    return 420;
}
```