# flake.nix
{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";
    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
      in
      {
        devShell = pkgs.mkShell {
          name = "Dev Shell";
          buildInputs = with pkgs; [
	    nodejs
	    bun
	    firebase-tools
	    # jetbrains.webstorm
            # (python3.withPackages (p: with p; with python3Packages; []))
          ];
          shellHook = '''';
        };
      }
    );
}
