# demo-todolist

This is a demo todo list application.

## Installing and setting up Vue
### 1. Install NodeJs
Follow the official installation guide on the [Node website](https://nodejs.org/en/).

### (optional) 2. Install Bun
Replace `bun` with `npm`in the following snippets if you skip this step
```sh
sudo apt install bun
```

## Create a simple Vue project
### 1. Initialize Vue
You cannot create the project in the current directory (just move the files after initialization)
```
bun create vuetify
```
We are using Vue 2.x because Vuetify is not yet compatible with Vue 3.x. 
Everything else is basically up to you. I do not recommend Typescript for beginners, 
ESLint can be irritating especially during debug phases because it throws *too many errors*.
```
? Project name: <project-name>

? Which preset would you like to install?: Default (Vuetify)        
? Use TypeScript? › Yes                                              
? Would you like to install dependencies with yarn, npm, pnpm, or bun?: bun (or npm)
? Install Dependencies?: Yes
```
Navigate into your project directory.

### 2. Run your App
(With Webstorm) Inside your `package.json`, click ![run][run] next to `"dev": "vite"` 
(i.e. `script.dev`). This will run the `dev` script with npm.
Alternatively, run
```sh
bun run dev
```


Then click on the link that is displayed. (Probably something like [localhost:8080](http://localhost:8080))


### 3. Implement your app
#### Folder structure / boilerplate
After initializing your app with Vue and adding Vuetify, your project structure should look something like this:  
📦  
┣ 📂 dist *(later)*   
┣ 📂 node_modules  
┣ 📂 public  
┣ 📂 src  
┃ ┣ 📂 assets  
┃ ┣ 📂 components  
┃ ┃ ┗ 📜 HelloWorld.vue  
┃ ┣ 📂 plugins  
┃ ┣ 📜 App.vue  
┃ ┗ 📜 main.js  
┗ 📜 package.json

**App.vue** is where your application lives. We can think of this like a main class, while it is controlling almost
everything on a high level, we won't be doing much here besides importing and using our components.  

**components** is where our components live, we can easily add new Single File Components (SFC, .vue-files) here 
and use then in our app or other components. You can also create subdirectories for namespacing purposes.  

**HelloWorld.vue** is an example component created by Vue that was overwritten by Vuetify to give a template for the
framework. This is where we will do most of our work by using the given template, modifying and renaming it to fit our
needs.

**assets** is where you would put things like images you want to use in your app.

The **package.json** file specifies many metadata attributes for our app. Including dependencies, which can be thought
of as the libraries we are using inside the app to extend its functionality, and a couple of scripts to ease the
development progress. If there is any command which you find yourself quite often, simply giving it a name and add it here.
These scripts can be run using `bun run <script>`.

**node_modules** is where the dependencies are stored. Simply delete this directory if your dependencies behave weirdly
and get it back by running `bun install` (`bun i`). This can also be deleted to temporarily preserve space while your 
app is not running.

**dist** is where the build will go, this is what you would publish on a webserver. Don't worry if this directory is not
there yet, it will be created later.

#### Implementation (for real)
Go to the HelloWorld.vue component and delete everything inside the `v-container` component (also known as the root 
component because it is and always will be the only component directly inside our component template).
Then, add your first text to the root component:
```vue
<p>
  Hello World
</p>
```

After running your app, you should see the updated text in your browser. Vue supports hot-reload meaning there is no
need to refresh the browser page every time. Webstorm will also automatically save your work when tabbing out.  

You are now ready to add your first real content: Simply add the following code inside the root component 
(nesting two `v-container` components):
```vue
<!-- mt is a simple styling class that provides a way to easily add margin or padding -->
<!-- The name is comprised or either m (margin) or p (padding) followed by the direction, these include -->
<!-- t (top), b (bottom), l (left), r (right), a (all sides), x (x-direction), y (y-direction) -->
<!-- The number specifies the size from 1 thru 10 -->
<v-container class="mt-10">
  <h1 class="display-2 font-weight-bold mb-8">
    Welcome to your Todo List
  </h1>
  <p>
    Get things done
  </p>
</v-container>
```
This will do just what it says and add some text to the app.
We can extend this by adding reactive content that changes based on the app's state. Start by registering a couple variables inside the script tag:
```js

const todos = ref([
    {text: "Zähne putzen", done: false},
    {text: "Duschen", done: false},
])

const newTodo = ref()

```

Our reactive text could now look something like this:
```vue
<h1 class="mb-10">
Welcome to your Todo List
</h1>
<p v-if="todos.length === 0">
You don't appear to have any tasks 🤔
</p>
<p v-else-if="todos.find(v => !v.done)">
<!-- Insert dynamic values into your page by using double curly braces {{ }} -->
You have completed {{ todos.reduce((acc, v) => acc + (v.done ? 1 : 0), 0) }} of {{ todos.length }}
{{ todos.length !== 1 ? 'tasks' : 'task' }}.
</p>
<p v-else>
You have completed all of your tasks. Great job 🎉
</p>
```

Let's add another bit to show the current todo items:
```vue
<h2 class="headline font-weight-bold mb-3">
What's left to do
</h2>

<!-- Insert all uncompleted items -->
<!-- A card is usually a good component to just wrap something in for a kind of visual border -->
<v-card
  v-for="item in todos.filter(item => !item.done)"
  :key="item.name"
  class="my-2"
  @click="item.done =! item.done"
>
<v-card-title>
  <h4>{{ item.text }}</h4>
</v-card-title>
</v-card>
```

Add a couple of methods to work with the todo list:
```js
function addTodo() {
    // When accessing refs inside the script tag, we need to use the "value" property
    // The is not necessary in the template section
    todos.value.push({text: newTodo.value, done: false})
    newTodo.value = ""
}

// A function can be called in the template just ĺike you would expect
function deleteTodo(text: string) {
    todos.value = todos.value.filter(todo => todo.text !== text)
}

function clearTodos() {
    todos.value = []
}
```

Now we can add a way to create a new task. Add this directly after the h2 tag:
```vue
<v-text-field
    class="mt-8"
    append-icon="mdi-plus"
    label="Add new Item"
    v-model="newTodo"
    @click:append="addTodo"
    @keydown.enter="addTodo"
/>
```

Next, you can copy the whole list and paste it again for the completed items. Simply adjust it so that it fits.

You can also add a button to delete all todos like this:
```vue
<v-btn
    :disabled="!todos.length"
    color="red"
    @click="clearTodos"
    prepend-icon="mdi-delete"
>
  Delete All
</v-btn>
```

Finally, let's add a way to automatically save the items. Add this inside the `script` tag:
```js
// A watcher will trigger whenever the value of the watched value changes
// The "deep" option is necessary since we never change the actual value of "todos",
// but only the items *inside* todos.value
watch(todos, saveState, {deep: true})

function saveState() {
    localStorage.setItem('todos', JSON.stringify(todos.value))
}
```
And also make it load the saved items automatically when the page loads.
```js
// The onMounted hook is called when the site is loaded
// Database interactions should happen asynchronously (which also requires your app to be able to handle empty data)
onMounted(() => {
    if (localStorage.todos)
        todos.value = JSON.parse(localStorage.todos)
})
```

To finish up your component, refactor and rename it to something more fitting like `TodoList` (Webstorm: Shift+F6). 
Make sure to also change every reference inside the `App.vue`.

    


### 4. Deploy your app
If you get an error during build, try to remove the following lines from `types` inside `tsconfig.json`:
```json
"vite-plugin-vue-layouts/client",
"unplugin-vue-router/client"
```

Check out the [Firebase Hosting guide](https://gitlab.com/torbeneims/overview/-/tree/master/firebase-hosting)

[run]: https://resources.jetbrains.com/help/img/idea/2021.3/icons.actions.execute_dark.svg

## Use this project as is
### Install npm-dependencies
```
bun install
```

### Compiles and hot-reloads for development
```
bun run dev
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
